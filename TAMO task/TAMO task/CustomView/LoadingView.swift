//
//  LoadingView.swift
//  SingFit-Swift
//
//  Created by Mahamudul on 22/10/20.
//

import Foundation
import UIKit


final class LoadingView: UIView {
    
    static let shared = LoadingView()
    
    //MARK:- Show Loader
    func showLoader(sender: UIViewController?, msg: String = "Please wait..."){
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        sender?.present(alert, animated: true, completion: nil)
        
        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
            subview.layer.cornerRadius = 1
        subview.backgroundColor = UIColor.white
    }
    
    
    //MARK:- Hide Loader
    func hideLoader(sender: UIViewController?){
        sender?.dismiss(animated: false, completion: nil)
    }
}
