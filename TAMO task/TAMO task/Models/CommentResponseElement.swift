//
//  CommentResponseElement.swift
//  TAMO task
//
//  Created by Mahamudul on 1/3/21.
//

import Foundation

// MARK: - CommentResponseElement
struct CommentResponseElement: Codable {
    let id, eventID, eventComment: String?
}
