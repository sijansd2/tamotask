//
//  LoginResponseElement.swift
//  TAMO task
//
//  Created by Mahamudul on 27/2/21.
//

import Foundation


// MARK: - LoginResponseElement
struct LoginResponseElement: Decodable {
    
    let userID, authToken: String?
    let avatar: String?
    let firstName, lastName, email, password: String?
}

