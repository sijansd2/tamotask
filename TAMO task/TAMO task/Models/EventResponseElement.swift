//
//  EventResponseElement.swift
//  TAMO task
//
//  Created by Mahamudul on 28/2/21.
//

import Foundation

// MARK: - EventResponseElement
class EventResponseElement: Codable {
    var id, userID, eventDate, eventType: String?
    var eventSubject, eventAddress: String?
    var hasAttachment, hasLabel, hasVideo: Bool?
    var rating: String?
    var important: Bool?
    var isOngoing: Bool?
    var startHour, startMinute, endHour, endMinute, date, month, year: String?
    
}
