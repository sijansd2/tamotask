//
//  DateItem.swift
//  TAMO task
//
//  Created by Mahamudul on 27/2/21.
//

import Foundation
import UIKit

// MARK: - DateItem
class DateItem{
    
    var date: String? = nil
    var year: String? = nil
    var month: String? = nil
    var isCurrentDate: Bool = false
    var yearMonthLbl: UILabel? = nil
}
