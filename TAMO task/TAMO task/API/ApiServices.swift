//
//  ApiServices.swift
//  TAMO task
//
//  Created by Mahamudul on 27/2/21.
//

import Foundation
import UIKit


final class ApiServices{
    
    //Singletone
    static let shared = ApiServices()
    
    //api endpoints
    let loginApi = "/authenticate"
    let eventListApi = "/authenticate/1/events"
    let eventCommentApi = "/authenticate/1/events/"
    
    //MARK:-Login API
    func login(
        _ vc: UIViewController,
        completion: @escaping (Result<LoginResponseElement>) -> ()){
        
        //show loader
        LoadingView.shared.showLoader(sender: vc)
        
        let url = URL(string: Constants.baseUrl + loginApi)!

        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            
            do{
                let response = try JSONDecoder().decode([LoginResponseElement].self, from: data)
                debugPrint(response)
                DispatchQueue.main.async {
                    completion(.success(response.first!))
                }
            }
            catch let jsonError{
                debugPrint(jsonError)
                DispatchQueue.main.async {
                    completion(.failure(jsonError))
                }
            }
            
            //hide loader
            DispatchQueue.main.async {
                LoadingView.shared.hideLoader(sender: vc)
            }
        }

        task.resume()
    }
    
    
    
    
    
    //MARK:-Get Events API
    func getEventsById(
        _ vc: UIViewController,
        completion: @escaping (Result<[EventResponseElement]>) -> ()){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            //show loader
            LoadingView.shared.showLoader(sender: vc)
        })
        
        
        let url = URL(string: Constants.baseUrl + eventListApi)!

        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            
            do{
                var nEventList = [EventResponseElement]()
                let response = try JSONDecoder().decode([EventResponseElement].self, from: data)
                debugPrint(response)
                
                for event in response{
                    let item = MyCalendar.shared.getTimeinterval(eventItem: event)
                    nEventList.append(item)
                }
                
                nEventList = nEventList.sorted { (element1, element2) -> Bool in
                    return (element1.startHour! < element2.startHour!)
                }
                
                DispatchQueue.main.async {
                    completion(.success(nEventList))
                }
            }
            catch let jsonError{
                debugPrint(jsonError)
                DispatchQueue.main.async {
                    completion(.failure(jsonError))
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                //hide loader
                LoadingView.shared.hideLoader(sender: vc)
            })
        }

        task.resume()
    }
    
    
    
    
    //MARK:-Get Events Detail API
    func getEventDetailById(
        _ vc: UIViewController,
        eventId: String,
        completion: @escaping (Result<[CommentResponseElement]>) -> ()){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            //show loader
            LoadingView.shared.showLoader(sender: vc)
        })
        
        
        let url = URL(string: Constants.baseUrl + eventCommentApi + eventId + "/event")!

        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            
            do{
                let response = try JSONDecoder().decode([CommentResponseElement].self, from: data)
                debugPrint(response)
                
                DispatchQueue.main.async {
                    completion(.success(response))
                }
            }
            catch let jsonError{
                debugPrint(jsonError)
                DispatchQueue.main.async {
                    completion(.failure(jsonError))
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                //hide loader
                LoadingView.shared.hideLoader(sender: vc)
            })
        }

        task.resume()
    }
}
