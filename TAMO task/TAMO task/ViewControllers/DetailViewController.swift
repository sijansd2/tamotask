//
//  DetailViewController.swift
//  TAMO task
//
//  Created by Mahamudul on 1/3/21.
//

import UIKit

class DetailViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var eventSubject_lbl: UILabel!
    @IBOutlet weak var eventType_lbl: UILabel!
    @IBOutlet weak var address_lbl: UILabel!
    @IBOutlet weak var rating_lbl: UILabel!
    
    var subject, type, address, rating: String!
    var eventIdentifire: String!
    var comments: [CommentResponseElement]! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLabels()
        getEventComments()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


//MARK:- Class Extension
extension DetailViewController{
    
    //set labels
    func setLabels(){
        eventSubject_lbl.text = subject
        eventType_lbl.text = type
        address_lbl.text = address
        rating_lbl.text = rating
    }
    
    //get all comments
    func getEventComments(){
        
        guard MyReachability.isConnected() else {
            showAlert(title: "Error", msg: Constants.ALERT_NO_INTERNET)
            return
        }
        
        ApiServices.shared.getEventDetailById(self, eventId: eventIdentifire) { response in
            
            switch response{
            case .success(let response):
                self.comments = response
                self.tableView.reloadData()
                
            case .failure(let err):
                print(err)

            }
        }
    }
}


//MARK:- TableView Extension
extension DetailViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CommentCell.identifire, for: indexPath) as? CommentCell
        
        cell?.configure(item: comments[indexPath.row])
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
