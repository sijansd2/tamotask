//
//  ViewController.swift
//  TAMO task
//
//  Created by Mahamudul on 26/2/21.
//

import UIKit

class AuthViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var tf_userName: FormTextField!
    @IBOutlet weak var tf_password: FormTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    //MARK:- Click Events
    @IBAction func loginClicked(_ sender: Any) {
        
        guard checkValidation() else {
            return
        }

        doLogin()
    }
    
}

//MARK:- Class Extensions
extension AuthViewController{
    
    //Check validation
    private func checkValidation() -> Bool{
        
        guard MyReachability.isConnected() else {
            showAlert(title: "Error", msg: Constants.ALERT_NO_INTERNET)
            return false
        }
        
        guard (tf_userName.text != "") else{
            showAlert(msg: Constants.ALERT_USERNAME_EMPTY)
            return false
        }
        
        guard (tf_password.text != "") else{
            showAlert(msg: Constants.ALERT_PASSWORD_EMPTY)
            return false
        }
        
        return true
    }
    
    
    //call login api
    func doLogin(){
        
        ApiServices.shared.login(self) { response in
            
            switch response{
            case .success(let response):
                
                self.storeUserInfo(userInfo: response)
                self.gotoHomePage()
                
            case .failure(let err):
                print(err)

            }
        }
    }
    
    //store user info
    func storeUserInfo(userInfo: LoginResponseElement){
        MyUserDefaults.shared.saveString(key: MyUserDefaults.userId_key, value: userInfo.userID)
        MyUserDefaults.shared.saveString(key: MyUserDefaults.avatarUrl_key, value: userInfo.avatar)
    }
    
    
    //goto home page
    func gotoHomePage(){
        
        let story = UIStoryboard(name: "Home", bundle:nil)
        let vc = story.instantiateInitialViewController()
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
