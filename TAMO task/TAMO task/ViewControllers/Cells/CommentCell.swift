//
//  CommentCell.swift
//  TAMO task
//
//  Created by Mahamudul on 1/3/21.
//

import UIKit

class CommentCell: UITableViewCell {
    
    //cell identifire
    static let identifire = "CommentCell"

    //MARK:- variables
    @IBOutlet weak var comment_lbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    public func configure(item: CommentResponseElement){
        comment_lbl.text = item.eventComment
    }

}
