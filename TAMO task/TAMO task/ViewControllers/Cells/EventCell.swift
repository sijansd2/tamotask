//
//  EventCell.swift
//  TAMO task
//
//  Created by Mahamudul on 28/2/21.
//

import UIKit

class EventCell: UITableViewCell {
    
    //cell identifire
    static let identifire = "EventCell"
    
    //MARK:- Variables
    @IBOutlet weak var startTime_lbl: UILabel!
    @IBOutlet weak var endTime_lbl: UILabel!
    @IBOutlet weak var eventType_lbl: UILabel!
    @IBOutlet weak var eventSubject_lbl: UILabel!
    @IBOutlet weak var eventAddress_lbl: UILabel!
    @IBOutlet weak var rating_lbl: UILabel!
    @IBOutlet weak var hasLabel_img: UIImageView!
    @IBOutlet weak var attached_img: UIImageView!
    @IBOutlet weak var video_img: UIImageView!
    @IBOutlet weak var separetor1: UIView!
    @IBOutlet weak var separetor2: UIView!
    @IBOutlet weak var arraw_indicator: UIImageView!
    @IBOutlet weak var cardView: CardView!
    
    @IBOutlet weak var lbl_width_const: NSLayoutConstraint!
    @IBOutlet weak var attatch_width_const: NSLayoutConstraint!
    @IBOutlet weak var video_width_const: NSLayoutConstraint!
    
    let iconDefaultWidth = 43
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    public func configure(item: EventResponseElement){
        
        startTime_lbl.text = (item.startHour ?? "") + ":" + (item.startMinute ?? "")
        endTime_lbl.text = (item.endHour ?? "") + ":" + (item.endMinute ?? "")
        eventType_lbl.text = item.eventType
        eventSubject_lbl.text = item.eventSubject
        eventAddress_lbl.text = item.eventAddress
        rating_lbl.text = item.rating
        
        if item.isOngoing == true{
            cardView.borderWidth = 2
            arraw_indicator.isHidden = false
            eventType_lbl.textColor = UIColor.red
        }
        else{
            cardView.borderWidth = 0
            arraw_indicator.isHidden = true
            eventType_lbl.textColor = UIColor(rgb: 0x7B7B7B)
        }
        
        //label icon show/hide condition
        if item.hasLabel == false{
            hasLabel_img.isHidden = true
            lbl_width_const.constant = 0
        }
        else{
            hasLabel_img.isHidden = false
            lbl_width_const.constant = CGFloat(iconDefaultWidth)
        }
        
        //attachment icon show/hide condition
        if item.hasAttachment == false{
            attached_img.isHidden = true
            attatch_width_const.constant = 0
        }
        else{
            attached_img.isHidden = false
            attatch_width_const.constant = CGFloat(iconDefaultWidth)
        }
        
        //video icon show/hide condition
        if item.hasVideo == false{
            video_img.isHidden = true
            video_width_const.constant = 0
        }
        else{
            video_img.isHidden = false
            video_width_const.constant = CGFloat(iconDefaultWidth)
        }
        
        
        //seperator icon show/hide condition
        if item.hasLabel == false || item.hasAttachment == false{
            separetor1.isHidden = true
        }
        else{
            separetor1.isHidden = false
        }
        
        if item.hasVideo == false || item.hasAttachment == false{
            separetor2.isHidden = true
        }
        else{
            separetor2.isHidden = false
        }
        
        
        if (item.hasVideo == true && item.hasAttachment == true) ||
            (item.hasVideo == true && item.hasLabel == true){
            separetor2.isHidden = false
        }
        else{
            separetor2.isHidden = true
        }
        
        if (item.hasLabel == true && item.hasAttachment == true){
            separetor1.isHidden = false
        }
        else{
            separetor1.isHidden = true
        }
    }

}
