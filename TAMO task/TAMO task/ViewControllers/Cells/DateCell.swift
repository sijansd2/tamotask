//
//  DateCell.swift
//  TAMO task
//
//  Created by Mahamudul on 27/2/21.
//

import UIKit

class DateCell: UICollectionViewCell {
    
    //MARK:- Variables
    @IBOutlet weak var day_lb: UILabel!
    @IBOutlet weak var date_lb: UILabel!
    @IBOutlet weak var indicatorWidthConst: NSLayoutConstraint!
    
    @IBOutlet weak var weekEndIndicator: UIView!
    @IBOutlet weak var weekDayIndicator: DesignableView!
    
    let daysName = ["MON", "TUE", "WED", "THU", "FRI", "SAT","SUN"]
    var count = 0
    
    //cell identifire
    static let identifire = "DateCell"
    
    static func nib() -> UINib{
        return UINib(nibName: identifire, bundle: nil)
    }
    
    public func configure(dateitem: DateItem, position: Int){
        
        count = position
        
        if position >= 7{
            count = position % 7
        }
        
        if(dateitem.date == "04" || dateitem.date == "25"){
            dateitem.yearMonthLbl?.text = dateitem.year! + "  " + (dateitem.month?.uppercased())!
        }
        
        // condition for hilighting current date
        if dateitem.isCurrentDate{
            day_lb.textColor = UIColor.white
            date_lb.textColor = UIColor.white
        }
        else{
            day_lb.textColor = UIColor(rgb: 0x5D839A)
            date_lb.textColor = UIColor(rgb: 0x5D839A)
            indicatorWidthConst.constant = 4
        }
        
        //condition for show/hide bottom indicators
        if(dateitem.isCurrentDate && (daysName[count] == daysName[5] || daysName[count] == daysName[6])){
            weekEndIndicator.isHidden = false
            weekDayIndicator.isHidden = false
        }
        else if daysName[count] == daysName[5] || daysName[count] == daysName[6]{
            weekEndIndicator.isHidden = false
            indicatorWidthConst.constant = 0
        }
        else{
            weekEndIndicator.isHidden = true
            weekDayIndicator.isHidden = false
        }
        
        day_lb.text = daysName[count]
        date_lb.text = dateitem.date
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    
    //reset cell
    func resetCell(){
        indicatorWidthConst.constant = 4
        
        if daysName[count] == daysName[5] || daysName[count] == daysName[6]{
            weekEndIndicator.isHidden = false
            indicatorWidthConst.constant = 0
        }
    }
    
    //set selected item
    func selectItem(){
        indicatorWidthConst.constant = 47
    }

}
