//
//  HomeViewController.swift
//  TAMO task
//
//  Created by Mahamudul on 27/2/21.
//

import UIKit

class HomeViewController: UIViewController {
    
    //MARK:- Variables
    @IBOutlet weak var year_month_lb: UILabel!
    @IBOutlet weak var dateCollectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var avatar_imgv: UIButton!
    
    var allDates: [DateItem]!
    var allEvents: [EventResponseElement]? = nil
    var allEventsTemp: [EventResponseElement]? = nil
    var selectedIndexPath: IndexPath!
    var currentPosition: IndexPath!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        dateCollectionView.delegate = self
        dateCollectionView.dataSource = self
        tableView.separatorColor = UIColor.clear
        allDates = MyCalendar.shared.getAllDates()
        
        getAllEvents()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}


//MARK:- Class Extensions
extension HomeViewController{
    
    //Get all events by api call
    func getAllEvents(){
        
        guard MyReachability.isConnected() else {
            showAlert(title: "Error", msg: Constants.ALERT_NO_INTERNET)
            return
        }
        
        ApiServices.shared.getEventsById(self) { response in
            
            switch response{
            case .success(let response):
                self.allEvents = response
                self.allEventsTemp = self.allEvents
                self.loadDateNavigation()
                
            case .failure(let err):
                print(err)

            }
        }
    }
    
    //load top date navigation
    func loadDateNavigation(){
        
        let position = MyUserDefaults.shared.getInt(key: MyUserDefaults.currentDatePos_key)
        let indexPath = IndexPath(item: position ?? 0, section: 0)
        self.selectedIndexPath = indexPath
        self.dateCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        
        let selectedDate = self.allDates[indexPath.row].date
        let selectedMonth = self.allDates[indexPath.row].month
        self.allEventsTemp = self.allEvents?.filter{$0.date == selectedDate && $0.month == selectedMonth}
        self.tableView.reloadData()
    }
}


//MARK:- Collectionview Extensions
extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allDates.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let lastSelectedIndexPath = selectedIndexPath{
            let lastSelectedCell = self.dateCollectionView.cellForItem(at: lastSelectedIndexPath) as? DateCell
            lastSelectedCell?.selectItem()
        }
        
        let cell = dateCollectionView.dequeueReusableCell(withReuseIdentifier: DateCell.identifire, for: indexPath) as? DateCell
        
        let mDate = allDates[indexPath.row]
        mDate.yearMonthLbl = year_month_lb
        cell?.configure(dateitem: mDate, position: indexPath.row)
        
        if mDate.isCurrentDate{
            currentPosition = indexPath
        }
        
        return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let height = collectionView.frame.size.height
        let width = collectionView.frame.size.width  / 8.5
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let lastSelectedIndexPath = selectedIndexPath{
            let lastSelectedCell = self.dateCollectionView.cellForItem(at: lastSelectedIndexPath) as? DateCell
            lastSelectedCell?.resetCell()
        }
        
        selectedIndexPath = indexPath
        let cell = self.dateCollectionView.cellForItem(at: indexPath) as? DateCell
        cell?.selectItem()
        
        let selectedDate = self.allDates[indexPath.row].date
        let selectedMonth = self.allDates[indexPath.row].month
        self.allEventsTemp = self.allEvents?.filter{$0.date == selectedDate && $0.month == selectedMonth}
        self.tableView.reloadData()
    }
    
}


//MARK:- Collectionview Extensions
extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if allEventsTemp?.count == 0{
            self.tableView.setEmptyMessage("No Events Found")
        }
        else{
            self.tableView.restore()
        }
        
        return allEventsTemp?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: EventCell.identifire, for: indexPath) as? EventCell
        
        if let events = self.allEventsTemp{
            cell?.configure(item: events[indexPath.row])
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "detailSID") as! DetailViewController
        
        nextVC.eventIdentifire = allEventsTemp?[indexPath.row].id
        nextVC.subject = allEventsTemp?[indexPath.row].eventSubject
        nextVC.type = allEventsTemp?[indexPath.row].eventType
        nextVC.address = allEventsTemp?[indexPath.row].eventAddress
        nextVC.rating = allEventsTemp?[indexPath.row].rating
                
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
}
