//
//  MyCalendar.swift
//  TAMO task
//
//  Created by Mahamudul on 27/2/21.
//

import Foundation

class MyCalendar{
    
    //Singletone
    static let shared = MyCalendar()
    
    //MARK:- variables
    let startDate = "2021-February-01"
    let endDate = "2021-May-31"
    final let formate = "yyyy-MMMM-dd"
    final let formate2 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    let byAddingValueOnline: Calendar.Component = .minute
    let onlineInterval: Int = 40
    
    func getAllDates()-> [DateItem]{

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        var sdate = dateFormatter.date(from:startDate)!
        let edate = dateFormatter.date(from:endDate)!
        
        var dates = [DateItem]()
        var count = 0

        while sdate <= edate {
            
            let splitedDate = dateFormatter.string(from: sdate).split(separator: "-")
            
            let dateItem = DateItem()
            dateItem.date = String(splitedDate[2])
            dateItem.month = String(splitedDate[1])
            dateItem.year = String(splitedDate[0])
            
            if getCurrentDate() == dateFormatter.string(from: sdate){
                dateItem.isCurrentDate = true
                MyUserDefaults.shared.saveInt(key: MyUserDefaults.currentDatePos_key, value: count)
            }
            dates.append(dateItem)
            
            print(dateFormatter.string(from: sdate))
            sdate = Calendar.current.date(byAdding: .day, value: 1, to: sdate)!
            
            count += 1
        }
        
        return dates
    }
    
    
    //get current date in string
    func getCurrentDate() -> String {
        let todaysDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        let DateInFormat = dateFormatter.string(from: todaysDate as Date)

        return DateInFormat
    }
    
    
    //Check online validation
    func getTimeinterval(eventItem: EventResponseElement)-> EventResponseElement{
        
        let startTimeStr = eventItem.eventDate ?? "2021-02-24T12:43:44.815Z"
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate2
        let startDateTime = dateFormatter.date(from: startTimeStr)!
        let endDateTime = Calendar.current.date(byAdding: byAddingValueOnline, value: onlineInterval, to: startDateTime)!
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "GMT")!
        let sHour = calendar.component(.hour, from: startDateTime)
        let sMinute = calendar.component(.minute, from: startDateTime)
        let sDate = calendar.component(.day, from: startDateTime)
        
        calendar.timeZone = TimeZone(identifier: "GMT")!
        let eHour = calendar.component(.hour, from: endDateTime)
        let eMinute = calendar.component(.minute, from: endDateTime)
        
        
        let now = Date()//dateFormatter.date(from: "2021-02-23T14:10:44.815Z")!
        if (startDateTime < now && endDateTime > now) {
            eventItem.isOngoing = true
        }
        else{
            eventItem.isOngoing = false
        }
        
        let mDateFormatter = DateFormatter()
        mDateFormatter.dateFormat = "LLLL"
        let nameOfMonth = mDateFormatter.string(from: endDateTime)
        
        
        eventItem.startHour = String(sHour)
        eventItem.startMinute = String(sMinute)
        eventItem.endHour = String(eHour)
        eventItem.endMinute = String(eMinute)
        eventItem.date = String(sDate)
        eventItem.month = nameOfMonth
        
        debugPrint(endDateTime)
        
        return eventItem
    }
}
