//
//  Constants.swift
//  TAMO task
//
//  Created by Mahamudul on 27/2/21.
//

import Foundation

class Constants: NSObject{
    
    static let baseUrl = "https://6033d74f843b150017931b4a.mockapi.io/api/v1"

    static let ALERT_NO_INTERNET = "Network Error, you seem to be offline. Please try again when internet access is available."
    
    static let ALERT_USERNAME_EMPTY = "You have to complete 'Username/Email' field"
    
    static let ALERT_PASSWORD_EMPTY = "You have to complete 'Password' field"
}
